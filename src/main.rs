use clap::{ArgGroup, Parser};
use i2cdev::core::*;
use i2cdev::linux::{LinuxI2CDevice, LinuxI2CError, LinuxI2CMessage};

const DEFAULT_I2C_BUS: u8 = 1;
const DEFAULT_CHIP_ADDR: u16 = 0x53;

const REG_NAMES: &[&str] = &[
    "Control_1",
    "Control_2",
    "Control_3",
    "Control_4",
    "Control_5",
    "SR_Reset",
    "100th_Seconds",
    "Seconds",
    "Minutes",
    "Hours",
    "Days",
    "Weekdays",
    "Months",
    "Years",
    "Second_alarm",
    "Minute_alarm",
    "Hour_alarm",
    "Day_alarm",
    "Weekday_alarm",
    "CLKOUT_ctl",
    "Timestp_ctl1",
    "Sec_timestp1",
    "Min_timestp1",
    "Hour_timestp1",
    "Day_timestp1",
    "Mon_timestp1",
    "Year_timestp1",
    "Timestp_ctl2",
    "Sec_timestp2",
    "Min_timestp2",
    "Hour_timestp2",
    "Day_timestp2",
    "Mon_timestp2",
    "Year_timestp2",
    "Timestp_ctl3",
    "Sec_timestp3",
    "Min_timestp3",
    "Hour_timestp3",
    "Day_timestp3",
    "Mon_timestp3",
    "Year_timestp3",
    "Timestp_ctl4",
    "Sec_timestp4",
    "Min_timestp4",
    "Hour_timestp4",
    "Day_timestp4",
    "Mon_timestp4",
    "Year_timestp4",
    "Aging_offset",
    "INT_A_MASK1",
    "INT_A_MASK2",
    "INT_B_MASK1",
    "INT_B_MASK2",
    "Watchdg_tim_ctl",
    "Watchdg_tim_val",
];

// Disable manual_strip Clippy warning.
// In parse_prefixed_int() it is not possible to do it the suggested way.
#[allow(clippy::manual_strip)]

/// Convert a string slice to an integer, the base is determine from the prefix.
///
/// The string may contain 0b (for binary), 0o (for octal), 0x (for hex) or no
/// prefix (for decimal) values.
/// # Examples
///
/// ```
/// assert_eq!(parse_prefixed_int("0xA"), Ok(10));
/// ```
fn parse_prefixed_int<T>(src: &str) -> Result<T, String>
where
    T: num::Unsigned + num::Num<FromStrRadixErr = std::num::ParseIntError>,
{
    let val = if src.starts_with("0b") {
        T::from_str_radix(&src[2..], 2)
    } else if src.starts_with("0o") {
        T::from_str_radix(&src[2..], 8)
    } else if src.starts_with("0x") {
        T::from_str_radix(&src[2..], 16)
    } else {
        T::from_str_radix(src, 10)
    };
    match val {
        Ok(val) => Ok(val),
        Err(e) => Err(format!("{e}")),
    }
}

#[test]
fn test_parse_prefixed_int() {
    assert_eq!(parse_prefixed_int::<u8>("0xA"), Ok(10));
    assert_eq!(parse_prefixed_int::<u16>("0xA"), Ok(10));
    assert_eq!(parse_prefixed_int::<u32>("0xA"), Ok(10));
    assert_eq!(parse_prefixed_int::<u64>("0xA"), Ok(10));
    assert_eq!(parse_prefixed_int("0b1010"), Ok(10u16));
    assert_eq!(parse_prefixed_int("0o12"), Ok(10u16));
    assert_eq!(parse_prefixed_int("10"), Ok(10u16));
    assert_eq!(parse_prefixed_int("0"), Ok(0u16));
    assert_eq!(parse_prefixed_int("010"), Ok(10u16));
    assert_eq!(parse_prefixed_int("0xffff"), Ok(u16::MAX));
    assert_eq!(parse_prefixed_int("0xffffffff"), Ok(u32::MAX));
    assert_eq!(parse_prefixed_int("0xffffffffffffffff"), Ok(u64::MAX));
    assert_eq!(
        parse_prefixed_int::<u16>("0x10000"),
        Err("number too large to fit in target type".to_string())
    );
    assert_eq!(
        parse_prefixed_int::<u16>("-1"),
        Err("invalid digit found in string".to_string())
    );
}

fn parse_reg(src: &str) -> Result<(u8, Option<u8>), String> {
    match src.split_once("=") {
        None => {
            let reg = parse_prefixed_int(src)?;
            if reg as usize >= REG_NAMES.len() {
                return Err(format!("Unknown register: {:#04x}", reg));
            }
            Ok((reg, None))
        }
        Some((reg, val)) => {
            let reg = parse_prefixed_int(reg)?;
            if reg as usize >= REG_NAMES.len() {
                return Err(format!("Unknown register: {:#04x}", reg));
            }
            let val = parse_prefixed_int(val)?;
            Ok((reg, Some(val)))
        }
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(group(ArgGroup::new("asdf").required(true).args(["reg", "print_all"])))]
struct Cli {
    #[clap(short, long, default_value_t = DEFAULT_I2C_BUS, value_parser = parse_prefixed_int::<u8>)]
    i2c_bus: u8,
    #[clap(short, long, default_value_t = DEFAULT_CHIP_ADDR, value_parser = parse_prefixed_int::<u16>)]
    chip_addr: u16,
    #[clap(short, long)]
    print_all: bool,
    /// Address of the register to print or write. To write append =VAL to the reg address
    /// (example: 0x04=0x01).
    #[clap(value_parser = parse_reg)]
    reg: Option<Vec<(u8, Option<u8>)>>,
}

fn pcf2131_init(i2c_dev: u8, i2c_addr: u16) -> Result<Pcf2131, LinuxI2CError> {
    let dev = unsafe { LinuxI2CDevice::force_new(format!("/dev/i2c-{}", i2c_dev), i2c_addr) }?;
    Ok(Pcf2131 { i2c_dev: dev })
}

struct Pcf2131 {
    i2c_dev: LinuxI2CDevice,
}

fn print_reg(reg_num: usize, val: u8) {
    if reg_num > REG_NAMES.len() {
        panic!("FIX ME: Add error handling!");
    }

    println!(
        "{:#04x} {: <16} {:#04x}",
        reg_num,
        format!("{}:", REG_NAMES[reg_num]),
        val
    );
}

fn print_reg_regs(reg_num: usize, regs: &[u8]) {
    if reg_num as usize > regs.len() {
        panic!("FIX ME: Add error handling!");
    }

    let mut reg_name_max = 0;
    for s in REG_NAMES {
        if s.len() > reg_name_max {
            reg_name_max = s.len()
        }
    }
    print_reg(reg_num, regs[reg_num])
}

impl Pcf2131 {
    pub fn read_reg(&mut self, addr: u8) -> Result<u8, LinuxI2CError> {
        let mut read_data = [0; 1];
        let mut msg = [
            LinuxI2CMessage::write(&[addr]),
            LinuxI2CMessage::read(&mut read_data),
        ];
        self.i2c_dev.transfer(&mut msg)?;
        Ok(read_data[0])
    }

    pub fn write_reg(&mut self, addr: u8, val: u8) -> Result<(), LinuxI2CError> {
        let mut msg = [LinuxI2CMessage::write(&[addr, val])];
        self.i2c_dev.transfer(&mut msg)?;
        Ok(())
    }

    pub fn print_regs(&mut self) -> Result<(), LinuxI2CError> {
        let mut read_data = [0; 0x37];
        let mut msg = [
            LinuxI2CMessage::write(&[0x00]),
            LinuxI2CMessage::read(&mut read_data),
        ];
        self.i2c_dev.transfer(&mut msg)?;

        for i in 0..0x37 {
            print_reg_regs(i, &read_data);
        }
        Ok(())
    }
}

fn main() {
    let cli = Cli::parse();

    println!("/dev/i2c-{}: {:#04x}", cli.i2c_bus, cli.chip_addr);

    let mut pcf2131 = match pcf2131_init(cli.i2c_bus, cli.chip_addr) {
        Ok(pcf2131) => pcf2131,
        Err(e) => {
            eprintln!(
                "ERROR: Can't access device {:#04x}@i2c-{}: {e}",
                cli.chip_addr, cli.i2c_bus
            );
            std::process::exit(-1);
        }
    };

    if cli.print_all {
        pcf2131.print_regs().unwrap();
        return;
    }

    if let Some(regs) = cli.reg {
        for reg in regs {
            match reg {
                (addr, None) => {
                    let val = pcf2131.read_reg(addr).unwrap();
                    print_reg(addr as usize, val);
                }
                (addr, Some(val)) => {
                    println!(
                        "Write {:#04x} to register {:#04x} {}",
                        val, addr, REG_NAMES[addr as usize]
                    );
                    pcf2131.write_reg(addr, val).unwrap();
                }
            }
        }
    }
}
